-- LUALOCALS < ---------------------------------------------------------
local ItemStack, math, minetest, nodecore, pairs, string, tonumber,
      vector
    = ItemStack, math, minetest, nodecore, pairs, string, tonumber,
      vector
local math_exp, math_pi, math_random, string_format
    = math.exp, math.pi, math.random, string.format
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- DATABASE

local modname = minetest.get_current_modname()
local modstore = minetest.get_mod_storage()

local db = modstore:get_string("db")
if db == "" then db = nil end
db = db and minetest.deserialize(db)
db = db or {}

local function savedb()
	return modstore:set_string("db", minetest.serialize(db))
end

local function savestate(player, taken)
	local ent = {
		pos = player:get_pos(),
		inv = {},
		taken = taken
	}
	ent.pos.y = ent.pos.y + 1
	local inv = player:get_inventory()
	for i = 1, inv:get_size("main") do
		ent.inv[i] = inv:get_stack("main", i):to_string()
	end
	db[player:get_player_name()] = ent
end

------------------------------------------------------------------------
-- HUDS

local huddist = tonumber(minetest.settings:get(modname .. "_distance")) or 4
local huds = {}

local function hudfind(player, peer, name, mod)
	name = name or player:get_player_name()
	local h = huds[name]
	if not h then
		h = {}
		huds[name] = h
	end
	h[peer] = mod(h[peer])
end

local function lineofsight(viewer, viewee)
	viewer.y = viewer.y + 1.625
	for pt in minetest.raycast(viewer, viewee, true, true) do
		if pt.type == "node" then
			return
		end
	end
	return true
end

minetest.register_globalstep(function()
		local pmap = {}
		for _, p in pairs(minetest.get_connected_players()) do
			pmap[p:get_player_name()] = p
		end
		for name, player in pairs(pmap) do
			local pos = player:get_pos()
			for peer, entry in pairs(db) do
				if (not pmap[peer])
				and (vector.distance(entry.pos, pos) <= huddist)
				and lineofsight(pos, entry.pos) then
					hudfind(player, peer, name, function(i)
							if i then return i end
							return player:hud_add({
									hud_elem_type = "waypoint",
									world_pos = db[peer].pos,
									name = "[" .. peer .. "]",
									text = "",
									number = 0xffffff
								})
						end)
				else
					hudfind(player, peer, name, function(i)
							if i and player then player:hud_remove(i) end
						end)
				end
			end
		end
	end)

------------------------------------------------------------------------
-- INVENTORY ENTITIES

local cbs = 0.125
minetest.register_entity(modname .. ":slotent", {
		initial_properties = nodecore.stackentprops(),
		is_yctiwy = true,
		pname = "",
		slot = 1,
		loose = 0,
		yctiwy_check = function(self)
			if minetest.get_player_by_name(self.pname) then
				return self.object:remove()
			end
			local ent = db[self.pname]
			local stack = ent.inv and ent.inv[self.slot]
			if ent.taken and ent.taken[self.slot] then stack = "" end
			stack = ItemStack(stack or "")
			local props = nodecore.stackentprops(stack, nil, 0.05)
			props.visual_size.x = props.visual_size.x / 2
			props.visual_size.y = props.visual_size.y / 2
			if props.is_visible then
				props.collisionbox = {-cbs, -cbs, -cbs, cbs, cbs, cbs}
			end
			if not self.yawed then
				self.yawed = true
				self.object:set_yaw(math_random() * math_pi * 2)
			end
			self.object:set_properties(props)
		end,
		on_activate = function(self)
			self.cktime = 0.00001
		end,
		on_step = function(self, dtime)
			self.loose = self.loose / math_exp(dtime / 5)
			self.cktime = (self.cktime or 0) - dtime
			if self.cktime > 0 then return end
			self.cktime = 1
			return self:yctiwy_check()
		end,
		on_punch = function(self, puncher)
			self.loose = self.loose + 1
			if self.loose < 4 then return end
			local ent = db[self.pname]
			local stack = ent.inv and ent.inv[self.slot]
			if ent.taken and ent.taken[self.slot] then stack = "" end
			stack = ItemStack(stack or "")
			if stack:is_empty() then return end
			minetest.log(string_format("%s: %q steals %q slot %d item %q",
					modname, puncher:get_player_name(), self.pname,
					self.slot, stack:to_string()))
			ent.taken = ent.taken or {}
			ent.taken[self.slot] = true
			savedb()
			stack = puncher:get_inventory():add_item("main", stack)
			if not stack:is_empty() then
				nodecore.item_eject(self.object:get_pos(), stack)
			end
			return self.object:remove()
		end
	})

local function findents(pos, cb)
	for _, o in pairs(minetest.get_objects_inside_radius(pos, 2)) do
		local l = o and o.get_luaentity and o:get_luaentity()
		if l and l.is_yctiwy then cb(l) end
	end
end

local s = 0.25
local offsets = {
	{x = -s, y = -s, z = -s},
	{x = s, y = -s, z = -s},
	{x = -s, y = s, z = -s},
	{x = s, y = s, z = -s},
	{x = -s, y = -s, z = s},
	{x = s, y = s, z = s},
	{x = -s, y = s, z = s},
	{x = s, y = -s, z = s},
}

local function spawninv(name, entry)
	entry = entry or db[name]
	if not entry then return end
	local exist = {}
	findents(entry.pos, function(o)
			if o.pname == name then exist[o.slot] = true end
		end)
	for i = 1, #offsets do
		if not exist[i] then
			local p = vector.add(offsets[i], entry.pos)
			local o = minetest.add_entity(p, modname .. ":slotent")
			o = o and o:get_luaentity()
			if o then
				o.pname = name
				o.slot = i
			end
		end
	end
end

------------------------------------------------------------------------
-- PLAYER HOOKS

minetest.register_on_leaveplayer(function(player)
		huds[player:get_player_name()] = nil
		savestate(player)
		return savedb()
	end)

minetest.register_on_shutdown(function()
		for _, pl in pairs(minetest.get_connected_players()) do
			savestate(pl)
		end
		return savedb()
	end)

minetest.register_on_joinplayer(function(player)
		local name = player:get_player_name()
		local ent = db[name]
		if (not ent) or (not ent.taken) then return end
		local inv = player:get_inventory()
		for k in pairs(ent.taken) do
			inv:set_stack("main", k, "")
		end
		findents(player:get_pos(), function(o)
				if o.pname == name then o.object:remove() end
			end)
	end)

------------------------------------------------------------------------
-- TIMER

local timer = 0
minetest.register_globalstep(function(dtime)
		timer = timer - dtime
		if timer > 0 then return end
		timer = 3 + math_random() * 2

		local rollcall = {}
		for _, pl in pairs(minetest.get_connected_players()) do
			rollcall[pl:get_player_name()] = true
			savestate(pl)
		end

		for name, ent in pairs(db) do
			if not minetest.player_exists(name) then
				db[name] = nil
			elseif not rollcall[name] then
				spawninv(name, ent)
			end
		end

		return savedb()
	end)
